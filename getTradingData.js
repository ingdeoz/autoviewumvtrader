if(document.getElementsByClassName("alert-list")[1]){
  console.log('Listado de Alertas detectado correctamente');

  if(document.getElementsByClassName("alert-list")[1].length == 0){
    console.log("No hay Registro de Alertas desplegado o no contiene alertas");
  }else{
    console.log('\n\n');
    console.log("Revisando el Registro de Alertas...");
   
    var registroAlertas = document.getElementsByClassName("alert-list")[1].innerText.split('\n');
    var resumen = ['','','','','',''];

    if(registroAlertas[0] == 'Symbol\tDescription\tDate/Time'){
      console.log('Registro de alertas visible');
    
      for(i = 2; i<23;i=i+5){
        //resumen[(i+3)/5] = registroAlertas[i].trim() + '|' + registroAlertas[i+2].trim() + '|' + parseFecha(registroAlertas[i+3].trim());
        resumen[(i+3)/5] = registroAlertas[i+2].trim() + '|' + parseFecha(registroAlertas[i+3].trim());
      }
    }else{
      console.log('Registro de alertas oculto');
      var registroAlertas = registroAlertas[0].substring(26,registroAlertas[0].length);
      //console.log(registroAlertas);
      var registroAlertas2 = registroAlertas.split('|');
      console.log("0: " + registroAlertas2[0]);
      console.log("1: " + registroAlertas2[1]);
      console.log("2: " + registroAlertas2[2]);
      console.log("3: " + registroAlertas2[3]);
      console.log("4: " + registroAlertas2[4]);
      console.log("5: " + registroAlertas2[5]);
      console.log("6: " + registroAlertas2[6]);
      console.log("7: " + registroAlertas2[7]);
      console.log("8: " + registroAlertas2[8]);
      console.log("9: " + registroAlertas2[9]);
      console.log("10: " + registroAlertas2[10]);
      console.log("11: " + registroAlertas2[11]);
      for(i=0;i<17;i=i+4){
        var accion = '';
        var fecha = '';
        if(registroAlertas2[i+3].trim().substring(0,3) == "BUY"){
          accion = "BUY";
          fecha = registroAlertas2[i+4].trim().substring(3,12);
        }else if (registroAlertas2[i+3].trim().substring(0,3) == "SELL"){
          accion = "SELL"
          fecha = registroAlertas2[i+4].trim().substring(4,12);
        }

        if(i==0){
          resumen[(i+4)/4] = registroAlertas2[i].trim() + '|' + registroAlertas2[i+1].trim() + '|' + registroAlertas2[i+2].trim() + '|' + accion +  '|' + parseFecha(fecha);
        }else{
          resumen[(i+4)/4] = registroAlertas2[i].split(':')[1].substring(2,12).trim() + '|' + registroAlertas2[i+1].trim() + '|' + registroAlertas2[i+2].trim() + '|' + accion +  '|' + parseFecha(fecha);
        }
      }
    }

    console.log('\nResumen: \n');
    for(i=1 ; i <= resumen.length-1 ; i++){
      console.log(resumen[i]);
    }

        // Read it using the storage API
      chrome.storage.sync.get(['ultima_entrada'], function(items) {
        var ultima_entrada = JSON.parse(JSON.stringify(items)).ultima_entrada;
        //console.log('ultima_entrada: ' + ultima_entrada);

        for(i=1 ; i <= resumen.length-1 ; i++){
          console.log('\n'+ultima_entrada);
          console.log(resumen[i]);
          if(ultima_entrada==resumen[i]){
            //Son iguales
            console.log('Son iguales');
            break;
          }else{
            console.log('Son distintos. Enviar a API y actualizar localStorage');
            executeApiRest(resumen[i]);
            chrome.storage.sync.set({'ultima_entrada': resumen[i]}, function() {
              console.log('Nueva alerta guardada en localStorage');
          });
        }
      }
    });
  }
}else{
  console.log('Existe más de una ventana de tradingview abierta');
}

  function parseFecha(fechaTradingView){

    var fechaSplited = fechaTradingView.split(' ');
    console.log(fechaSplited);

    var f = new Date();

    var mes = '';
    switch(fechaSplited[0]){
      case 'Jan': mes = '01'; break;
      case 'Feb': mes = '02'; break;
      case 'Mar': mes = '03'; break;
      case 'Apr': mes = '04'; break;
      case 'May': mes = '05'; break;
      case 'Jun': mes = '06'; break;
      case 'Jul': mes = '07'; break;
      case 'Aug': mes = '08'; break;
      case 'Sep': mes = '09'; break;
      case 'Oct': mes = '10'; break;
      case 'Nov': mes = '11'; break;
      case 'Dec': mes = '12'; break;
    }
    console.log("mes: " + mes);

    var dia = (fechaSplited[1].length==1) ? '0' + fechaSplited[1] : fechaSplited[1];
    //console.log(dia);

    fechaParseada = f.getFullYear() + mes + dia + fechaSplited[2].substring(0,5).replace(':','');
    console.log("fechaParseada: " + fechaParseada);

    return fechaParseada; 
  }

  function executeApiRest(body) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
             alert(this.responseText);
         }
    };
    xhttp.open("POST", "https://baf01c08.ngrok.io/api/autoview", true);
    xhttp.setRequestHeader("Content-type", "application/json","");
    var request = '{"value": "' + body + '"}';
    console.log('request: ' + request);
    xhttp.send(request);
}
