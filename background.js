
chrome.tabs.query({'url': "https://*.tradingview.com/*"}
  , function(tabs) {
      tbs = tabs[0];
      if (tabs.length == 1) {
//        alert("Automated TradingView Extension is running");
      } else {
  //      alert("Please have one and only one tradingview chart page opened.");
      }
    });

var f = new Date();
var hora_inicial = f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();

console.log('Se inicia')

chrome.storage.sync.set({'ultima_entrada': 'inicial'}, function() {
       console.log('Alerta inicial guardada en localStorage.');
      });

chrome.tabs.onUpdated.addListener( function(request, sender, sendResponse) {

    var f_actual = new Date();
    var hora_actual = f_actual.getHours() + ":" + f_actual.getMinutes() + ":" +  f_actual.getSeconds();

    if(f_actual.getSeconds() % 1 == 0){
      console.log("----------------------------------\navanzamos 5 segundos " + hora_actual);
      hora_inicial = hora_actual;

      chrome.tabs.query({'url': "https://*.tradingview.com/*"}, function(tabs) {
        //console.log(tabs.length);
        if(tabs.length >= 1){
          console.log('Existen ' + tabs.length + ' tabs de tradingview abiertas');
          chrome.tabs.executeScript(tabs[0].id, {file: "getTradingData.js"});
        } else {
          console.log('No existen tabs de tradingview abiertas');
        }
      });

    }else{
       console.log('mismo segundo');
    }
  });
